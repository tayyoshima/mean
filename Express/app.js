var express = require('express');
var app = express();
var server = require('http').Server(app);
const user = require("./user");

app.get('/', function(req, res){
    res.end("yes you can do");
});

app.get('/ac?cd',function(req,res){
    res.end("this is a abcd route");
});

app.get('/users/:userId/books/:bookId', function(req, res){
    let userId = req.params.userId;
    let bookId = req.params.bookId;
    res.end('userId = '+userId);
});

app.use('/user', user);

server.listen(8000);
//app.use(express.static('web'));
console.log("server running port 8000");