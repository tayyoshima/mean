var express = require('express');
var router = express.Router();

router.get('/',function(req,res){
    res.end("get user");
});

router.post('/',function(req,res){
    res.end("create user");
});

router.put('/',function(req,res){
    res.end("update user");
});

router.delete('/',function(req,res){
    res.end("delete user");
});

module.exports = router;
// https://gitlab.com/sommai.k/simple2